package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Activity_2 {
    public static void main(String[] args) {

        int[] primeNumbers = new int[5];

        primeNumbers[0] = 2;
        primeNumbers[1] = 3;
        primeNumbers[2] = 5;
        primeNumbers[3] = 7;
        primeNumbers[4] = 11;

        System.out.println("The first prime number is " + primeNumbers[0]);
        System.out.println("The second prime number is " + primeNumbers[1]);
        System.out.println("The third prime number is " + primeNumbers[2]);
        System.out.println("The fourth prime number is " + primeNumbers[3]);
        System.out.println("The fifth prime number is " + primeNumbers[4]);

        ArrayList<String> friendList = new ArrayList<String>();

        friendList.add("John");
        friendList.add("Jane");
        friendList.add("Chloe");
        friendList.add("Zoey");

        System.out.println("My friends are: " + friendList);

        HashMap<String, Integer> hygieneList = new HashMap<String, Integer>() {
            {
                put("toothpaste", 15);
                put("toothbrush", 20);
                put("soap", 12);
            }
        };

        System.out.println("Our current inventory consists of: " + hygieneList);
    }
}
